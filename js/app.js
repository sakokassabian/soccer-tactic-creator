//Problem: No user interaction causes no change to application
//Solution: When user interacts cause changes appropriately
var color = $(".selected").css("background-color");


//When clicking on control list items
	$(".controls ul>li").click(function(){
		//deselect sibling elements
		$(this).siblings().removeClass("selected");
		//Select clicked elements
		$(this).addClass("selected");
		//cache the color
	color = $(this).css("background-color");
	});
	

//When new color is pressed
	$("button#revealColorSelect").click(function(){
		//Show color select or hide the color select	
		$("div#colorSelect").toggle();
	});
	
//update the new color span
function changeColor() {
	var r = $("input#red").val();
	var g = $("input#green").val();
	var b = $("input#blue").val();
	$("#newColor").css("background-color", "rgb(" + r + "," + g + "," + b + ")");
}
	//When color sliders changes
	$("input[type=range]").change(changeColor);
//When add color is pressed
	//Append the color to the controls ul
	//Select the new color

//On mouse events on the canvas
	//draw lines